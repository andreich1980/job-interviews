# Job Interview project

## Installation

```bash
git clone git@bitbucket.org:andreich1980/job-interviews.git
cd job-interviews
composer i
cp .env.example .env
php artisan key:generate
touch database/database.sqlite
php artisan migrate --seed
```
